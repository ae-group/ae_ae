features and use-cases
**********************

the portions of this namespace are simplifying your Python application or service in the areas/domains::

    * data processing/validation
    * file handling
    * i18n (localization)
    * configuration settings
    * console
    * logging
    * database access
    * networking
    * multi-platform/-OS
    * context help
    * app tours
    * user preferences (font size, color, theming, ...)
    * QR codes
    * sideloading

there are :doc:`some screenshots <screenshots>` of app screens done with the help of ae portions.
