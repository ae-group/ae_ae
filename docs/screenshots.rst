:orphan:

screenshots of apps developed with ae portions
**********************************************


.. list-table::

    * - .. figure:: img/glsl_tester_demo.gif
           :scale: 90 %

           GlslTest app demo


.. list-table::

    * - .. figure:: img/kivy_lisz_root.png
           :alt: root list of a dark themed kivy lisz app
           :scale: 30 %

           kivy lisz app root list

      - .. figure:: img/kivy_lisz_fruits.png
           :alt: fruits sub-list of a dark themed kivy lisz app
           :scale: 30 %

           fruits sub-list

      - .. figure:: img/kivy_lisz_fruits_light.png
           :alt: fruits sub-list of a light themed kivy lisz app
           :scale: 30 %

           using light theme

    * - .. figure:: img/kivy_lisz_user_prefs.png
           :alt: user preferences drop down
           :scale: 30 %

           lisz user preferences

      - .. figure:: img/kivy_lisz_color_editor.png
           :alt: kivy lisz color editor
           :scale: 30 %

           kivy lisz color editor

      - .. figure:: img/kivy_lisz_font_size_big.png
           :alt: lisz app using bigger font size
           :scale: 30 %

           bigger font size


.. list-table::
    :widths: 27 66

    * - .. figure:: img/enaml_lisz_fruits_sub_list.png
           :alt: fruits sub-list of dark themed enaml lisz app
           :scale: 27 %

           enaml/qt lisz app

      - .. figure:: img/enaml_lisz_light_landscape.png
           :alt: fruits sub-list of a light themed enaml lisz app in landscape
           :scale: 66 %

           light themed in landscape
